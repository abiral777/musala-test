package com.abiral.musalasoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abiral.musalasoft.domain.Medication;

import java.util.Optional;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

    Optional<Medication> findByCode(String code);

}
