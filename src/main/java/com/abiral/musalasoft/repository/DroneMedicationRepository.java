package com.abiral.musalasoft.repository;

import java.util.List;

import com.abiral.musalasoft.domain.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abiral.musalasoft.domain.Drone;
import com.abiral.musalasoft.domain.DroneMedication;

public interface DroneMedicationRepository extends JpaRepository<DroneMedication, Long> {
	
    List<DroneMedication> findByDrone(Drone drone);

    DroneMedication findByMedicationAndDrone(Medication medication, Drone drone);

}
