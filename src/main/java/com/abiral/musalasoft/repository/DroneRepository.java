package com.abiral.musalasoft.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abiral.musalasoft.domain.Drone;

public interface DroneRepository extends JpaRepository<Drone, Long>{
	
	Optional<Drone> findBySerialNumber(String serial);

}
