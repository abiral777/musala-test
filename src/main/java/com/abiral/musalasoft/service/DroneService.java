package com.abiral.musalasoft.service;

import java.util.List;
import java.util.Map;

import com.abiral.musalasoft.dto.DroneDTO;
import com.abiral.musalasoft.dto.DroneMedicationDTO;
import com.abiral.musalasoft.dto.MedicationDTO;
import com.abiral.musalasoft.dto.RegisterDTO;

public interface DroneService {
	
	 boolean isDroneModel(String type);
	 
	 boolean isDroneState(String type);
	 
	 DroneDTO droneRegistration(RegisterDTO registerDTO);
	 
	 DroneMedicationDTO loadingDrone(List<DroneMedicationDTO> dto, long droneId);

	List<Map<String,Object>> availableDronesForLoading();

	Integer retrieveBatteryLevel(long droneId);

}
