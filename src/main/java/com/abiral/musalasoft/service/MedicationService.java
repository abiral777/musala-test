package com.abiral.musalasoft.service;

import java.util.List;
import java.util.Map;

import com.abiral.musalasoft.domain.Medication;
import com.abiral.musalasoft.dto.MedicationDTO;
import com.abiral.musalasoft.dto.RegisterMedicationDTO;
import org.springframework.web.multipart.MultipartFile;

public interface MedicationService {
	List<MedicationDTO> viewDroneMedicationItems(long droneId);

	RegisterMedicationDTO addMedicationItem(Medication medication, MultipartFile file);

}
