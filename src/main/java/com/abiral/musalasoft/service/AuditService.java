package com.abiral.musalasoft.service;

public interface AuditService {

    void runBatteryAudit();

    void chargeDroneBattery();
}
