package com.abiral.musalasoft.domain;

public enum DroneState {
	
	IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
