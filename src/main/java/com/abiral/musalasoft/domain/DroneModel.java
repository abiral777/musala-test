package com.abiral.musalasoft.domain;

public enum DroneModel {
	lightweight, middleweight, cruiserweight, heavyweight
}
