package com.abiral.musalasoft.controller;

import com.abiral.musalasoft.domain.Medication;
import com.abiral.musalasoft.dto.DroneDTO;
import com.abiral.musalasoft.dto.DroneMedicationDTO;
import com.abiral.musalasoft.dto.RegisterDTO;
import com.abiral.musalasoft.dto.RegisterMedicationDTO;
import com.abiral.musalasoft.service.DroneService;
import com.abiral.musalasoft.service.MedicationService;
import com.abiral.musalasoft.utils.ResponseBuilder;
import com.abiral.musalasoft.utils.ResponseBuilder.ResponseCode;
import com.abiral.musalasoft.utils.ResponseBuilder.ResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
public class DispatchController {
	
	@Autowired
	private DroneService droneService;

    @Autowired
    private MedicationService medicationService;

	
	@PostMapping(value= "/drones")
	public Map<String, Object> droneRegistration(@RequestBody RegisterDTO dto) {
		try {           
            DroneDTO registeredDrone = droneService.droneRegistration(dto);
            return ResponseBuilder.buildObjectResponse(registeredDrone, ResponseCode.OK_STATUS_CODE, ResponseType.SUCCESS, "Successfully Registered Drone.");
        } catch (RuntimeException e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseCode.INVALID_INPUT_STATUS_CODE, ResponseType.ERROR, e.getMessage());
        }
    }
	
	@PostMapping(value= "/drones/loading/{droneId}")
	public Map<String, Object> droneLoading(@RequestBody List<DroneMedicationDTO> dto, @PathVariable Long droneId) {
		try {           
           
            return ResponseBuilder.buildObjectResponse(droneService.loadingDrone(dto, droneId), ResponseCode.OK_STATUS_CODE, ResponseType.SUCCESS, "Successfully Loaded Drone.");
        } catch (RuntimeException e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseCode.INVALID_INPUT_STATUS_CODE, ResponseType.ERROR, e.getMessage());
        }
    }

    @GetMapping(value= "/drones/loading")
    public Map<String, Object> availableDronesForLoading() {
        try {

            return ResponseBuilder.buildObjectResponse(droneService.availableDronesForLoading(), ResponseCode.OK_STATUS_CODE, ResponseType.SUCCESS, "All available drones for loading");
        } catch (RuntimeException e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseCode.INVALID_INPUT_STATUS_CODE, ResponseType.ERROR, e.getMessage());
        }
    }

    @PostMapping(value= "/drones/{droneId}")
    public Map<String, Object> viewDroneMedicationItems(@PathVariable Long droneId) {
        try {

            return ResponseBuilder.buildListResponse(medicationService.viewDroneMedicationItems(droneId), ResponseCode.OK_STATUS_CODE, ResponseType.SUCCESS, "Medication Items");
        } catch (RuntimeException e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseCode.INVALID_INPUT_STATUS_CODE, ResponseType.ERROR, e.getMessage());
        }
    }

    @GetMapping(value= "/drones/{droneId}/battery")
    public Map<String, Object> viewBatteryPercentagge(@PathVariable Long droneId) {
        try {

            Integer battery= droneService.retrieveBatteryLevel(droneId);
            return ResponseBuilder.buildListResponse(battery, ResponseCode.OK_STATUS_CODE, ResponseType.SUCCESS, "Current Battery Level: "+battery);
        } catch (RuntimeException e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseCode.INVALID_INPUT_STATUS_CODE, ResponseType.ERROR, e.getMessage());
        }
    }

    @PostMapping(value = "/medication", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> addMedicationItem(@RequestBody Medication medication, @RequestParam MultipartFile imageFile) {
        try {
            RegisterMedicationDTO dto= medicationService.addMedicationItem(medication,imageFile);
            return ResponseBuilder.buildListResponse(dto, ResponseCode.OK_STATUS_CODE, ResponseType.SUCCESS, "New Medication Loaded ");
        } catch (RuntimeException e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseCode.INVALID_INPUT_STATUS_CODE, ResponseType.ERROR, e.getMessage());
        }
    }
}
